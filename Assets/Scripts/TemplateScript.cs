﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateScript : MonoBehaviour
{
    [SerializeField]
    private GameObject finalObject;

    private Vector2 mousePosition;

    [SerializeField]
    private LayerMask groupTilesLayer;

    void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(Mathf.Round(mousePosition.x), Mathf.Round(mousePosition.y));

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D rayHit = Physics2D.Raycast(transform.position, Vector2.zero, Mathf.Infinity, groupTilesLayer);

            if (rayHit.collider == null)
            {
                Instantiate(finalObject, transform.position, Quaternion.identity);
            }
        }
        else if (Input.GetKeyDown("space"))
        {
            RaycastHit2D rayHit = Physics2D.Raycast(transform.position, Vector2.zero, Mathf.Infinity, groupTilesLayer);

            if (rayHit.collider != null)
            {
                Destroy(rayHit.collider.gameObject);
            }
        }
    }
}
