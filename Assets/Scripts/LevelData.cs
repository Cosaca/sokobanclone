﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData
{
    public float[] levelObjectsPosition;
    /*public float[] treePosition;
    public float[] boxPosition;
    public float[] goalPosition;*/
    
    public LevelData(GameObject[] position)
    {
        levelObjectsPosition = new float[3];
        levelObjectsPosition[0] = position[0].transform.position.x;
        levelObjectsPosition[1] = position[1].transform.position.y;
        levelObjectsPosition[2] = position[2].transform.position.z;
    }
}
