﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EditorMenu : MonoBehaviour
{
    public Image saveButton;
    public Image loadButton;

    private GameObject[] levelObjects;

    public static bool gameIsPaused;
    public GameObject editorMenu;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        editorMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Pause()
    {
        editorMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void Menu()
    {
        SceneManager.LoadScene("Home");
        Time.timeScale = 1f;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Load()
    {
        SaveSystem.LoadLevelData();
        loadButton.color = Color.grey;
        loadButton.GetComponent<Button>().enabled = false;
    }

    public void Save()
    {
        levelObjects = FindObjectsOfType<GameObject>();

        SaveSystem.SaveLevel(levelObjects);

        saveButton.color = Color.grey;
        saveButton.GetComponent<Button>().enabled = false;
    }
}
