﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeMenu : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene("1");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Controls()
    {
        SceneManager.LoadScene("Controls");
    }

    public void LevelEditor()
    {
        SceneManager.LoadScene("Editor");
    }

    public void LoadLevelEditor()
    {
        SceneManager.LoadScene("Test1");
    }
}
