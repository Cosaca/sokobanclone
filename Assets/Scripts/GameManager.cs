﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text score;
    public Text boxNumber;
    public GameObject winPanel;
    private AudioSource[] sounds;

    private bool hasWon;

    GameObject[] boxes;

    void Start()
    {
        sounds = FindObjectsOfType<AudioSource>();

        Time.timeScale = 1f;
        hasWon = false;
        
        Data.boxScore = 0;
        winPanel.SetActive(false);

        boxes = GameObject.FindGameObjectsWithTag("Box");
        boxNumber.text = boxes.Length.ToString();
    }

    void Update()
    {
        score.text = Data.boxScore.ToString();

        if(!hasWon)
        {
            CheckWin();
        }
    }

    void CheckWin()
    {
        if (Data.boxScore == boxes.Length)
        {
            hasWon = true;
            winPanel.SetActive(true);
            Time.timeScale = 0f;
            StopSounds();
        }
    }

    void StopSounds()
    {
        foreach (AudioSource item in sounds)
        {
            item.Stop();
        }
    }
}
