﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveLevel(GameObject[] levelObjectsPos)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/level.txt";
        FileStream stream = new FileStream(path, FileMode.Create);

        LevelData data = new LevelData(levelObjectsPos);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static LevelData LoadLevelData()
    {
        string path = Application.persistentDataPath + "/level.txt";

        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            LevelData data = formatter.Deserialize(stream) as LevelData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Fallo" + path);
            return null;
        }
    }
}
