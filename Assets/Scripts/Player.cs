﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 2f;
    public float maxSpeed = 3f;

    Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Movement();
    }

    public void Movement()
    {
        Vector3 direction = Vector3.zero;

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            if(Input.GetAxisRaw("Horizontal") > 0)
            {
                direction = Vector3.right;
                transform.eulerAngles = Vector3.forward * 90;
            }
            else
            {
                direction = Vector3.left;
                transform.eulerAngles = Vector3.forward * -90;
            }

        }
        else if (Input.GetAxisRaw("Vertical") != 0)
        {
            if (Input.GetAxisRaw("Vertical") > 0)
            {
                direction = Vector3.up;
                transform.eulerAngles = Vector3.forward * -180;
            }
            else
            {
                direction = Vector3.down;
                transform.eulerAngles = Vector3.forward * 0;
            }
        }

        rb.AddForce(direction * speed);

        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }
}
