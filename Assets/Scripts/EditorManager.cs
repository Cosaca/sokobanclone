﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorManager : MonoBehaviour
{
    private int selectedObject;
    private GameObject currentObject;

    [SerializeField]
    private GameObject[] objects;
    private List<GameObject> objectsInLevel;

    private bool isObjectSelected;

    void Start()
    {
        selectedObject = 0;
        objectsInLevel = new List<GameObject>();
    }

    void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 objectPosition = new Vector2(Mathf.Round(mousePosition.x), Mathf.Round(mousePosition.y));

        if (Input.GetKeyDown("e") && isObjectSelected == false)
        {
            currentObject = Instantiate(objects[selectedObject], objectPosition, Quaternion.identity);
            isObjectSelected = true;
        }

        if (Input.GetMouseButtonDown(1) && isObjectSelected == true)
        {
            Destroy(currentObject);
            isObjectSelected = false;
            selectedObject = 0;
        }

        foreach (GameObject item in objectsInLevel)
        {
            Debug.Log(item);
            /*if (item.CompareTag("Goal") || item.CompareTag("Tree"))
            {
                objectsInLevel.Add(item);
            }*/
        }

        ChangeBetweenObjects(objectPosition);
    }

    private void ChangeBetweenObjects(Vector2 objectPosition)
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && isObjectSelected == true)
        {
            selectedObject++;
            if (selectedObject > objects.Length - 1)
            {
                selectedObject = 0;
            }

            Destroy(currentObject);
            currentObject = Instantiate(objects[selectedObject], objectPosition, Quaternion.identity);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f && isObjectSelected == true)
        {
            selectedObject--;
            if (selectedObject < 0)
            {
                selectedObject = objects.Length - 1;
            }

            Destroy(currentObject);
            currentObject = Instantiate(objects[selectedObject], objectPosition, Quaternion.identity);
        }
    }
}
