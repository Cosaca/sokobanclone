# PEC 3 - SokobanClone

## Objetivo
Esta práctica se ha diseñado con la intención de desarrollar un clon del famoso juego "Sokoban", que consiste en llevar las cajas de cada nivel a una meta, con lo que se completaría dicho nivel. En este caso se han estructurado 10 niveles, de manera que haya una progresión entre los mismos, y suponga un reto de cara al jugador.

## Estructura
El proyecto se estructura de la siguiente manera:

- Escena Home

Encargada de mostrar la pantalla principal del videojuego, así como las diferentes opciones disponibles para el usuario.

- Escena Controls

Esta escena muestra los controles y las instrucciones para resolver los diferentes niveles, y también los controles referentes tanto a los niveles como al editor de niveles.

- Escena Editor (sin terminar)

Escena encargada de tener el editor de niveles del juego, la cual no está terminada. Actualmente solo se pueden colocar objetos para diseñar un nivel completo, pero no se puede guardar como un nivel nuevo.

- Escena Cargar nivel (desactivado)

La intención de esta escena sería la de cargar el nivel previamente guardado en la escena anterior. Está desactivada por defecto, debido a que no está completo el modo editor.

- Escenas 1-10

Las diferentes escenas que comprenden los 10 niveles del videojuego.

## Tareas por realizar

Finalizar funcionalidad de editor del videojuego, por lo que esto no está disponible, únicamente los niveles diseñados en las diferentes escenas, de manera que se pueda jugar al menos a los niveles actualmente visibles para el jugador.

### Creador: Samuel Valcarcel Arce 